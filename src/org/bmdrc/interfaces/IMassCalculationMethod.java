/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2014, SungBo Hwang <tyamazaki@naver.com>.
 */
package org.bmdrc.interfaces;

/**
 *
 * @author SungBo Hwang <tyamazaki@naver.com>
 */
public interface IMassCalculationMethod {

    String COMPARE_20_AND_500_SCAN = "Compare 20 and 500 Scan";
}

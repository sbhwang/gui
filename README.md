# README #

This is BMDRC Library for AHeDD Research

Now, AHeDD is the java library for researches of natural products such as Asia Hubs

Basically, It supports computational techniques as SQL DB management, Mass Spectrum Analysis, Molecular Formula Generation and etc.

And We will support many functions for solving numerical problems for finding unknown compounds in Natural products.

The goal of this project is to support tools for the research of natural products for drug discovery.